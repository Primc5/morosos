//
//  TaskCell.swift
//  toDoList
//
//  Created by PABLO CRUZ MENDEZ on 19/12/18.
//  Copyright © 2018 PABLO CRUZ MENDEZ. All rights reserved.
//

import UIKit

class PersonCell: UITableViewCell {

    @IBOutlet var imgCell: UIImageView!
    @IBOutlet var textCell: UILabel!
    @IBOutlet var textF: UITextField!
    internal var repository = Repo()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        textF.isHidden = true
        textCell.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(lblTapped))
        tapGesture.numberOfTapsRequired = 1
        textCell.addGestureRecognizer(tapGesture)
        
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(textTapped))
        tapGesture2.numberOfTapsRequired = 2
        textF.addGestureRecognizer(tapGesture2)
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    @objc func lblTapped(){
        textCell.isHidden = true
        textF.isHidden = false
        textF.text = textCell.text
    }
    
    @objc func textTapped() {
        textF.isHidden = true
        textCell.isHidden = false

        
        let task = repository.exist(identifier: textCell.text!)
        task?.name = textF.text
        repository.update(a: task!)
        
        /*
        if repository.update(a: task){
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }*/
        
        textCell.text = textF.text
    }
 
}
