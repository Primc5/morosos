//
//  Resposiroey.swift
//  toDoList
//
//  Created by PABLO CRUZ MENDEZ on 19/12/18.
//  Copyright © 2018 PABLO CRUZ MENDEZ. All rights reserved.
//

import Foundation

protocol Protocolo{
    associatedtype T
    
    func getAll() -> [T]
    func getAllMorosos() -> [T]
    func get(identifier: String) -> T?
    func exist(identifier: String) -> T?
    func create(a: T) -> Bool
    func update(a: T) -> Bool
    func delete(a: T) -> Bool
}
