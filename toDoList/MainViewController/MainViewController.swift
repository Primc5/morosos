//
//  MainViewController.swift
//  toDoList
//
//  Created by PABLO CRUZ MENDEZ on 19/12/18.
//  Copyright © 2018 PABLO CRUZ MENDEZ. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet var table: UITableView!
    internal var tasks: [Person] = []
    internal var repository = Repo()
    var mode = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        tasks = repository.getAll()
        
        let addBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addPressed))
        navigationItem.setRightBarButton(addBarButtonItem, animated: false)
        let addBarButtonFilter = UIBarButtonItem(title: "Filter", style: .plain, target: self, action: #selector(filter))
        navigationItem.setLeftBarButton(addBarButtonFilter, animated: false)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        table.backgroundView = UIImageView(image: UIImage(named: "background"))
    }
    
    internal func registerCells(){
        let nib = UINib(nibName: "PersonCell", bundle: nil)
        table.register(nib, forCellReuseIdentifier: "PersonCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc internal func addPressed(){
        let addVC = AppViewController(task: nil)
        addVC.delegate = self
        //addVC.modalTransitionStyle = .coverVertical
        addVC.modalPresentationStyle = .overCurrentContext
        present(addVC, animated: true, completion: nil)
    }

    @objc internal func filter(){
        if(mode == 1){
            tasks = repository.getAllMorosos()
            table.reloadData()
            mode = 0
        }
        else{
            tasks = repository.getAll()
            table.reloadData()
            mode = 1
        }
    }


}

extension MainViewController: UITableViewDelegate, UITableViewDataSource{

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    
    func numberOfSection(in tableView: UITableView) -> Int{
        return 1
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PersonCell = tableView.dequeueReusableCell(withIdentifier: "PersonCell", for: indexPath) as! PersonCell
        let task = tasks[indexPath.row]
        cell.textCell.text = task.name
        cell.imgCell.isHidden = !task.isMoroso
        print("celula metida")
        cell.backgroundColor = .clear
        cell.backgroundColor = UIColor(white: 0, alpha: 0.2)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        tasks = repository.getAll()
        let task = tasks[indexPath.row]
        task.isMoroso = !task.isMoroso
        if repository.update(a: task){
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete{
            let task = tasks[indexPath.row]
            if repository.delete(a: task){
                tasks.remove(at: indexPath.row)
                tableView.beginUpdates()
                tableView.deleteRows(at: [indexPath], with: .automatic)
                tableView.endUpdates()
            }
        }
    }
    
    /*
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool{
        return true
    }
    */
    
}

extension MainViewController: AddViewControllerDelegate{
    func addViewController(_ vc: AppViewController, didEditTask task: Person){
        vc.dismiss(animated: true, completion: nil)
        if ((repository.exist(identifier: task.name)) == nil){
            if repository.create(a: task){
                tasks = repository.getAll()
                table.reloadData()
            }
        }
    }
}
