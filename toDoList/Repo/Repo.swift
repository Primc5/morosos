//
//  Repo.swift
//  toDoList
//
//  Created by PABLO CRUZ MENDEZ on 19/12/18.
//  Copyright © 2018 PABLO CRUZ MENDEZ. All rights reserved.
//

import UIKit
import RealmSwift

class Repo: Protocolo {
    
    func getAllMorosos() -> [Person] {
        var task: [Person] = []
        do{
            let entities = try Realm().objects(PersonEntity.self).filter("isMoroso == false")
            for entity in entities{
                let model = entity.taskModel()
                task.append(model)
            }
        }
        catch let error as NSError{
            print("Error getAll Tasks:" + error.description)
        }
        return task
    }
    
    
    func getAll() -> [Person]{
        var task: [Person] = []
        do{
            let entities = try Realm().objects(PersonEntity.self).sorted(byKeyPath: "created", ascending: false)
            for entity in entities{
                let model = entity.taskModel()
                task.append(model)
            }
        }
        catch let error as NSError{
            print("Error getAll Tasks:" + error.description)
        }
        return task
    }
    
    
    func get(identifier: String) -> Person? {
        do{
            let realm = try Realm()
            if let entity = realm.objects(PersonEntity.self).filter("id == %@", identifier).first {
                let model = entity.taskModel()
                return model
            }
        }
        catch{
            return nil
        }

        return nil
    }
    
    func exist(identifier: String) -> Person? {
        do{
            let realm = try Realm()
            if let entity = realm.objects(PersonEntity.self).filter("name == %@", identifier).first {
                let model = entity.taskModel()
                return model
            }
        }
        catch{
            return nil
        }
        
        return nil
    }
    
    func create(a: Person) -> Bool{
        do{
            let realm = try Realm()
            let entity = PersonEntity(task: a)
            try realm.write {
                realm.add(entity, update: true)
            }
        }
        catch{
            return false
        }
        return true
    }
    
    func update(a: Person) -> Bool{
        return create(a: a)
    }
    
    func delete(a: Person) -> Bool {
        do{
            let realm = try Realm()
            try realm.write {
                let entityToDelete = realm.objects(PersonEntity.self).filter("id == %@", a.id)
                realm.delete(entityToDelete)
            }
            
        }
        catch{
            return false
        }

        return true
    }
}
