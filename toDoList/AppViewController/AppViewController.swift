//
//  AppViewController.swift
//  toDoList
//
//  Created by PABLO CRUZ MENDEZ on 19/12/18.
//  Copyright © 2018 PABLO CRUZ MENDEZ. All rights reserved.
//

import UIKit

protocol AddViewControllerDelegate: class {
    func addViewController(_ vc: AppViewController, didEditTask task: Person)
}

class AppViewController: UIViewController {


    @IBOutlet weak var moroso: UISwitch!
    @IBOutlet weak var taxtField: UITextField!
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var saveButton: UIButton!
    
    internal var task: Person!
    weak var delegate: AddViewControllerDelegate!
    
    convenience init(task: Person?){
        self.init()
        if task == nil {
            self.task = Person()
            self.task.id = UUID().uuidString
            self.task.name = ""
            self.task.isMoroso = false
            self.task.created = Date()
        }
        else{
            self.task = task
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBack.layer.cornerRadius = 8.0
        viewBack.layer.masksToBounds = true
        
        saveButton.layer.cornerRadius = 8.0
        saveButton.layer.masksToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func exitButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        if(!taxtField.text!.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty){
            dismiss(animated: true, completion: nil)
            task.name = taxtField.text
            task.isMoroso = moroso.isOn
            task.created = Date()
            delegate.addViewController(self, didEditTask: task)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
