//
//  taskEntity.swift
//  toDoList
//
//  Created by PABLO CRUZ MENDEZ on 19/12/18.
//  Copyright © 2018 PABLO CRUZ MENDEZ. All rights reserved.
//

import UIKit
import RealmSwift

class PersonEntity: Object {
    @objc dynamic var id = ""
    @objc dynamic var name = ""
    @objc dynamic var isMoroso = false
    @objc dynamic var created = Date()
    
    override static func primaryKey() -> String?{
        return "id"
    }
    
    convenience init (task: Person){
        self.init()
        self.id = task.id
        self.name = task.name
        self.isMoroso = task.isMoroso
        self.created = task.created
    }
    
    
    func taskModel() -> Person{
        let model = Person()
        model.id = self.id
        model.name = self.name
        model.isMoroso = self.isMoroso
        model.created = self.created
        return model
    }
    
}
