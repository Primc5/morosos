//
//  Task.swift
//  toDoList
//
//  Created by PABLO CRUZ MENDEZ on 19/12/18.
//  Copyright © 2018 PABLO CRUZ MENDEZ. All rights reserved.
//

import Foundation

class Person{
    var id: String!
    var name: String!
    var isMoroso = false
    var created: Date!
}
